"""
.. module:: app_settings
   :synopsis: App specific settings which can be overridden by project settings.

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


from django.conf import settings
from uuid import uuid4


#: Callable used to generate the unique identifier for media models.
SM_MEDIA_UID = getattr(settings, "SM_MEDIA_UID", uuid4)


#: Defines the max length of the Media's uid field
SM_MEDIA_UID_MAX_LENGTH = getattr(settings, "SM_MEDIA_UID_MAX_LENGTH", 128)


#: Defines the max length of the Media's name field
SM_MEDIA_NAME_MAX_LENGTH = getattr(settings, "SM_MEDIA_NAME_MAX_LENGTH", 1024)


#: Defines the max length of the Media's name field
SM_MEDIA_DESCRIPTION_MAX_LENGTH = getattr(settings,
                                          "SM_MEDIA_DESCRIPTION_MAX_LENGTH",
                                          5120)


#: Callable used to generate the unique identifier for revision models.
SM_REVISION_UID = getattr(settings, "SM_REVISION_UID", uuid4)


#: Defines the max length of the Media's uid field
SM_REVISION_UID_MAX_LENGTH = getattr(settings, "SM_REVISION_UID_MAX_LENGTH",
                                     128)


#: Django storage engine to be used when storing revision files.  Defaults to
#: whatever the default file storage is.
SM_REVISION_STORAGE = getattr(settings, "SM_REVISION_STORAGE",
                              settings.DEFAULT_FILE_STORAGE)


#: Defiens the max length used for photo revision formats
SM_PHOTO_REVISION_FORMAT_MAX_LENGTH = getattr(settings,
                                          "SM_PHOTO_REVISION_FORMAT_MAX_LENGTH",
                                          10)


#: Callable used to generate the unique identifier for preview models.
SM_PREVIEW_UID = getattr(settings, "SM_REVISION_UID", uuid4)


#: Defines the max length of the Media's uid field
SM_PREVIEW_UID_MAX_LENGTH = getattr(settings, "SM_PREVIEW_UID_MAX_LENGTH", 128)


#: Django storage engine to be used when storing media preview files.  Defaults
#: to whatever the default file storage is.
SM_PREVIEW_STORAGE = getattr(settings, "SM_PREVIEW_STORAGE",
                             settings.DEFAULT_FILE_STORAGE)