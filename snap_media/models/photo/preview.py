"""
.. module:: preview
   :synopsis: Base model for Photo revision previews

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


from snap_media.models.abstract import preview as abstract_preview
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Preview(abstract_preview.Preview):
    """
    Photo specific preview implementation.
    """
    
    
    #: Specifies the width of the preview in pixels if it can be determined
    #: by the image processing dialect.
    width = models.IntegerField(help_text=_("Width of the preview in pixels"),
                                blank=False, null=False)
    
    
    #: Specifies the height of the preview in pixels if it can be determined
    #: by the image processing dialect.
    height = models.IntegerField(help_text=_("Height of the preview in pixels"),
                                 blank=False, null=False)