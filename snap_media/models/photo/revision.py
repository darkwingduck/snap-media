"""
.. module:: photo
   :synopsis: Base model for Photo media

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


from snap_media.models.abstract import revision as abstract_revision
from snap_media import app_settings as settings
from django.db import models


class Revision(abstract_revision.Revision):
    """
    Photo specific Revision implementation.
    """
    
    
    #: Specifies the format of the revision file.  It will be determined
    #: by getting the format from the image processing dialect.  If it cannot
    #: be determined, it's file extension will be stored in lowercase.
    format = models.CharField(help_text="Encoding format of the image.",
                      max_length=settings.SM_PHOTO_REVISION_FORMAT_MAX_LENGTH,
                      blank=False, null=False)
    
    
    #: Specifies the width of the revision in pixels if it can be determined
    #: by the image processing dialect.
    width = models.IntegerField(help_text="Width of the photo in pixels",
                                blank=False, null=True)
    
    
    #: Specifies the height of the revision in pixels if it can be determined
    #: by the image processing dialect.
    height = models.IntegerField(help_text="Height of the photo in pixels",
                                 blank=False, null=True)