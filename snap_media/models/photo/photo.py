"""
.. module:: photo
   :synopsis: Base model for Photo media

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


from snap_media.models.abstract.media import Media


class Photo(Media):
    """
    Media implementation specific to Photos and images.
    
    There is no real difference between a photo and media, so there are no model
    specific changes to be made.
    """
    pass