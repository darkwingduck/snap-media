"""
.. module:: preview
   :synopsis: Abstract base models for media previews

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


import snap_media.app_settings as settings
from django.db import models
from datetime import datetime
from snap_media.models.abstract.revision import Revision
from django.utils.translation import ugettext_lazy as _


class Preview(models.Model):
    """
    Abstract base model for revision previews of media.
    """
    
    class Meta:
        #: The Preview model should never be created on its own, only sub
        #: classes of Preview should have tables created.
        abstract = True
    
    
    #: Reference to the revision this preview was created from.
    revision = models.ForeignKey(Revision, help_text=_(("Revision this preview "
                                                      "was created from.")),
                                 null=False)
    
    
    #: Unique identifier for the preview.  Can be used as a public id since it
    #: can be specified in the settings for how it is generated.
    uid = models.CharField(help_text=_("Unique identifier"),
                           max_length=settings.SM_PREVIEW_UID_MAX_LENGTH,
                           blank=False, null=False, unique=True,
                           default=settings.SM_PREVIEW_UID)
    
    
    #: FileField that has access to the actual preview file information.
    file = models.FileField(help_text=_("Preview File"), null=False,
                            storage=settings.SM_PREVIEW_STORAGE)
    
    
    #: Datetime the preview was initially created.  It is automatically set when
    #: the model is created.
    created = models.DateTimeField(help_text=_(
                                           "Datetime the media was created"),
                                   auto_now=False, auto_now_add=True,
                                   blank=False, null=False)
    
    
    #: Datetime the media was deleted.  It is set when the delete method is
    #: called on the model.
    deleted = models.DateTimeField(help_text=_(
                                           "Datetime the media was deleted"),
                                   blank=False, null=True)
    
    
    def delete(self, *args, **kwargs):
        """
        Marks the media as deleted and sets it deleted datetime rather than
        actually removing it from the table.
        
        Args and Kwargs are passed through to the models save method.
        """
        
        self.deleted = datetime.now()
        self.save(*args, **kwargs)