"""
.. module:: revision
   :synopsis: Abstract base models for media revisions

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


import snap_media.app_settings as settings
from django.db import models
from datetime import datetime
from snap_media.models.abstract.media import Media
from django.utils.translation import ugettext_lazy as _


class Revision(models.Model):
    """
    Abstract base model for file revisions of media.
    """
    
    class Meta:
        #: The Revision model should never be created on its own, only sub
        #: classes of Revision should have tables created.
        abstract = True
    
    
    #: Reference to the Media instance this revision is for.
    media = models.ForeignKey(Media, help_text=_(("Reference to the Media "
                                                "instance this revision "
                                                "is associated with.")),
                              null=False)
    
    
    #: Unique identifier for the revision.  Can be used as a public id since it
    #: can be specified in the settings for how it is generated.
    uid = models.CharField(help_text=_("Unique identifier"), max_length=128,
                           blank=False, null=False, unique=True,
                           default=settings.SM_REVISION_UID)
    
    
    #: FileField that has access to the actual file information.
    file = models.FileField(help_text="Revision File", null=False,
                            storage=settings.SM_REVISION_STORAGE)
    
    
    #: Datetime the revision was initially created.  It is automatically set
    #: when the model is created.
    created = models.DateTimeField(help_text=_(("Datetime the revision was "
                                              "created")),
                                   auto_now=False, auto_now_add=True,
                                   blank=False, null=False)
    
    
    #: Datetime the revision was last updated.  It is automatically set every
    #: time the model saves.
    updated = models.DateTimeField(help_text=_(("Datetime the media was last "
                                              "updated")),
                                   auto_now=True, auto_now_add=False,
                                   blank=False, null=False)
    
    
    #: Datetime the revision was deleted.  It is set when the delete method is
    #: called on the model.
    deleted = models.DateTimeField(help_text=_(("Datetime the revsision was "
                                              "deleted")),
                                   blank=False,
                                   null=True)
    
    
    #: Is this revision the file being considered the best/most current version
    #: of the media.
    current = models.BooleanField(help_text=_(("Should this revision be used "
                                             "as the default media?")),
                                  null=False, default=False)
    
    
    def delete(self, *args, **kwargs):
        """
        Marks the revision as deleted and sets it deleted datetime rather than
        actually removing it from the table.
        
        Args and Kwargs are passed through to the models save method.
        """
        
        self.deleted = datetime.now()
        self.save(*args, **kwargs)