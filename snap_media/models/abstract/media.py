"""
.. module:: media
   :synopsis: Abstract base models for Media

.. moduleauthor:: Donald Ritter <donald.m.ritter@gmail.com>
"""


import snap_media.app_settings as settings
from django.db import models
from datetime import datetime
from django.utils.translation import ugettext_lazy as _


class Media(models.Model):
    """
    Abstract base model for different types of media.
    """
    
    class Meta:
        #: The Media model should never be created on its own, only sub classes
        #: of Media should have tables created.
        abstract = True

    
    #: Unique identifier for the media.  Can be used as a public id since it
    #: can be specified in the settings for how it is generated.
    uid = models.CharField(help_text=_("Unique identifier"),
                           max_length=settings.SM_MEDIA_UID_MAX_LENGTH,
                           blank=False, null=False, unique=True,
                           default=settings.SM_MEDIA_UID)
    
    
    #: Name displayed to help identify the media.
    name = models.CharField(help_text=_("Public name"),
                            max_length=settings.SM_MEDIA_NAME_MAX_LENGTH,
                            blank=True, null=False)
    
    
    #: Short description of the media to help identify it.
    description = models.CharField(help_text=_("Description"),
                       max_length=settings.SM_MEDIA_DESCRIPTION_MAX_LENGTH,
                       blank=True, null=False)
    
    
    #: Datetime the media was initially created.  It is automatically set when
    #: the model is created.
    created = models.DateTimeField(help_text=_(
                                           "Datetime the media was created"),
                                   auto_now=False, auto_now_add=True,
                                   blank=False, null=False)
    
    
    #: Datetime the media was last updated.  It is automatically set every time
    #: the model saves.
    updated = models.DateTimeField(help_text=_(("Datetime the media was last "
                                              "updated")),
                                   auto_now=True, auto_now_add=False,
                                   blank=False, null=False)
    
    
    #: Datetime the media was deleted.  It is set when the delete method is
    #: called on the model.
    deleted = models.DateTimeField(help_text=_(
                                           "Datetime the media was deleted"),
                                   blank=False, null=True)
    
    
    def delete(self, *args, **kwargs):
        """
        Marks the media as deleted and sets it deleted datetime rather than
        actually removing it from the table.
        
        Args and Kwargs are passed through to the models save method.
        """
        
        self.deleted = datetime.now()
        self.save(*args, **kwargs)