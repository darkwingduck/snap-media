import os
import inspect


#: Defines the absolute path of the package.  This is used instead of __dir__
#: because __dir__ may not be defined when testing something in idle.
PACKAGE_PATH = os.path.dirname(os.path.abspath(
                       inspect.getframeinfo(inspect.currentframe()).filename))


#: Defines the name of the package.  It is used by setup.py when creating eggs
#: and packaging source distributions.
PACKAGE_NAME = "Django-Snap-Media"


#: Defines the verion of the package.  It is used by setup.py when creating
#: eggs.
VERSION = "0.0.2"


__version__ = VERSION


#: Defines the description of the package.  It is used by setup.py when creating
#: eggs.
SHORT_DESCRIPTION = ("Django app that handles revisions of uploaded media. can "
                     "create previews of media, and can group media into list.")


#: Defines the long description of the package.
LONG_DESCRIPTION = ""


try:
    #only really need this for distributions
    with open(os.path.join(PACKAGE_PATH, "../README.rst"), "r") as f:
        LONG_DESCRIPTION = f.readlines()

except:
    pass


#: Defining the packages here so it can be used by other scripts to track
#: requirements.
REQUIRED_PACKAGES = [
    "Django>=1.3",
    "PIL>=1.1.7"
]


#: Defines all of the contributers to the library.  It is used by setup.py when
#: creating eggs.  Each author should be a tuple containing their name and
#: email. The first contributer in the list is used as the author in setup.py.
CONTRIBUTERS = [
    ("Donald Ritter", "donald.m.ritter@gmail.com"),
]


__author__ = CONTRIBUTERS[0][0]


#: Defines any keywords that can be used when searching pypi to help find this
#: package.
KEYWORDS = "django media revisions"

#: Defines the licensing of the package.
LICENSE = "BSD"