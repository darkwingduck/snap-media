Summary
=======
Django app that handles revisions of uploaded media, creating previews of media,
and can group media into list.

Roadmap
=======
* 0.1.0:
 
 - Create unit test for model CRUD operations.
 
 - Create unit test for generating PhotoPreviews.

* 0.0.4:

 - Create Album model that contains references to Media implementations and
   information like name, description, created, updated, deleted, uuid.

* 0.0.3:

 - Create method for Photo that can create a revision using a provided file.
 
 - Create method for PhotoRevision that can create a preview using a given file.
 
   - Create dialect dependent method to create actual preview image.
   
     - Create PIL dialect
 
 - Create method for PhotoPreview that can retrieve its actual preview image.

* 0.0.2: Current

 - Create Photo media type.
 
   - Create Photo model based on abstract Media model.
   
   - Create PhotoRevision model based on abstract Revision model.
   
   - Create PhotoPreview model based on abstract Preview model.

* 0.0.1:
 
 - Create project files like setup.py, README, MANIFEST.in.
 
 - Setup Sphinx for project documentation.
 
 - Create abstract Media model to store generic information like name,
   description, created, edited, deleted, and uuid.
 
 - Create abstract Revision model to store information like media reference,
   file, uploaded, size, current, format, and uuid.
 
 - Create abstract Preview model to store information like revision reference,
   format, size, and uuid.

License
=======
Copyright 2012 Donald Ritter. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

        Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.
        
        Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY DONALD RITTER ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL DONALD RITTER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of Donald Ritter.